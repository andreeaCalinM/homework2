const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

let products = [
    {
        id: 0,
        productName: 'Samsung Galaxy S9',
        price: 4000
    },
    {
        id: 1,
        productName: 'Iphone XR',
        price: 3999
    }
];

app.get('/get-all', (req, res) => {
    res.status(200).send(products);
})

app.post('/add', (req, res) => {
    if(req.body.productName && req.body.price){
        let product = {
            id: products.length,
            productName: req.body.productName,
            price: req.body.price
        };
        products.push(product);
        res.status(200).send(product);
    } else {
        res.status(500).send('Error!');
    }
});

app.put('/modifyProduct/:cod', (req, res) => {
 
   let put=false;
   
   products.forEach((product)=>{
         if(product.id==req.params.cod)
        {
          
              product.id=req.body.id;
             product.productName=req.body.productName;
            product.price=req.body.price;
           console.log(' s a modificat!');
           put=true;
         
        }
   });
     
        if(put==true){
             res.status(200).send("s-a modificat!");
        }
         else{ return res.status(500).send('Nu a fost gasit produsul!');
    
        }
 
    
  });
  
  
  app.delete('/deleteProduct', (req, res) => {
  const denumire=req.body.productName;
   let id=-2;

    products.forEach((product)=>{
         if(product.productName==denumire)
        {
          id=product.id;
             
         
        }
   });
   if(id==-2)
   {
        console.log(' nu s a sters!');
       return res.status(500).send('Nu a fost gasit produsul!');
    }
     else{ 
              products.splice(id,1);
              console.log(' s a sters!');
             res.status(200).send("s-a modificat!");
        }
        
     
});

app.listen(8080, () => {
    console.log('Server started on port 8080...');
});