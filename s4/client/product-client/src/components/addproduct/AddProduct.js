import React from 'react'
import axios from 'axios'

export class AddProduct extends React.Component
{
    
    constructor(props){
        //initialize the state of AddTodo component
        super(props);
        this.state={};
         this.state.id=0;
        this.state.productName='';
        this.state.price=0;
    }
  
    
     
    handleChangeName=(event)=>{
        this.setState({
            productName: event.target.value
        });
    }
    
     
    handleChangePrice=(event)=>{
        this.setState({
            price: event.target.value
        });
    }
    
    addProduct=()=>{
        let product={
          
            productName: this.state.productName,
            price: this.state.price
        }
       
        let axiosConfig = {
             headers: {
                "Content-Type": "application/json",
                 "Access-Control-Allow-Origin": "*",
                 "Access-Control-Allow-Methods": "GET, POST, OPTIONS, PUT, PATCH, DELETE"
                       }
                         };
             
        axios.post('https://homework2-andreeacalin.c9users.io:8081/add', product, axiosConfig)
        .then((res)=>{
          if(res.status===200){
              this.props.itemAdded(product);
          }
        }).catch((err)=>{
            console.log(err);
        })
    }
    
    render(){
        return(
            <div>
            <h1>Add Product</h1>
            <input type="text" placeholder="Product Name"
            value={this.state.productName}
            onChange={this.handleChangeName}/>
            <input type="text" placeholder="Price"
            value={this.state.price}
            onChange={this.handleChangePrice}/>
            <button onClick={this.addProduct}>Add Product</button>
            </div>
            
            );
    }
}