import React from 'react'


export class ProductList extends React.Component{
    constructor(props){
        super(props);
        this.state={};
        
    }
    
    render(){
        let items=this.props.source.map((item,index)=>{
           return <div key={index}>{item.productName}-{item.price} RON</div> 
            
        });
      return(
          <div>
          <h1>{this.props.title}</h1>
          <div>
          {items}
          </div>
          </div>
          );  
    }
}